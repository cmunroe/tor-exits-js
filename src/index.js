"use strict";
const r = require('axios');
var torIps;

function update(){
    return new Promise((resolve, reject) => {
        const secOps = r.get('https://raw.githubusercontent.com/SecOps-Institute/Tor-IP-Addresses/master/tor-exit-nodes.lst');
        const torProject = r.get('https://check.torproject.org/cgi-bin/TorBulkExitList.py?ip=8.8.8.8');
        Promise.all([secOps, torProject])
        .then(values => {
            var data;
            values.forEach(value => {
    
                data += value.data;
    
            });
            return data;
        })
        .then(values => {
            return values.split("\n")
        })
        .then(values => {
            torIps = values;
            resolve();
        })
        .catch(errors => {
            reject(errors)
        })

    })

}

function check(ip){
    if(torIps){
        return torIps.includes(ip);
    }
    else {
        return null;
    }
    
}


update();
setInterval(update, 1000 * 60 * 60 * 24);
module.exports.check = check;